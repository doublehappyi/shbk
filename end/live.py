# coding=utf-8
__author__ = 'db'

import os.path
import re
import datetime

import tornado.web
import settings
import base


class SubmitHandler(base.BaseHandler):
    @tornado.web.authenticated
    def get(self, *args, **kwargs):
        self.render("submit.html", user=self.get_user())

    @tornado.web.authenticated
    def post(self, *args, **kwargs):
        message = self.get_argument("message", "")
        if message == "":
            self.write("发稿文字不能为空")
            return
        elif len(message) > 200:
            self.write("发稿文字不能超过200个字")
            return
        anonymous = self.get_argument("anonymous", 0)
        if anonymous != 0:
            anonymous = 1

        try:
            photo = self.request.files["photo"]
            upload_path = os.path.join(settings.BASE_DIR, "front", "static", "photos")
            timestamp = re.compile("[\.:\- ]").sub("", str(datetime.datetime.now()))
            for meta in photo:
                file_name = meta['filename']
                file = meta["body"]
            photo_name = timestamp + "_" + file_name
            file_path = os.path.join(upload_path, photo_name)
            image = "/static/photos/" + photo_name
            with open(file_path, 'wb') as up:
                up.write(file)
        except:
            image = None

        db = self.get_db()
        ref_user = int(db.get("SELECT id FROM user WHERE username=%s", self.get_secure_cookie("username")).id)
        try:
            db.insert("INSERT INTO live VALUES (%s, %s, %s, %s, %s, %s)", None, message, anonymous, image, ref_user,
                      None)
            db.close()

            self.redirect("/")
        except:
            self.write("写入数据库失败")


class LiveHandler(base.BaseLiveHandler):
    @tornado.web.authenticated
    def get(self, _id):
        live_view = self.get_live_view(_id)
        comment_views = self.get_db().query("select * from comment_view where ref_live=%s", _id)
        self.get_db().close()
        self.render("live.html", user=self.get_user(), live_view=live_view, comment_views=comment_views)

    @tornado.web.authenticated
    def post(self, action):
        id = self.get_argument("id")
        if action == "delete":
            try:
                self.get_db().execute("DELETE FROM live WHERE id=%s and ref_user=%s", id, self.get_user().id)
                self.write({
                    "success":True,
                    "msg":"删除成功！"
                })
            except:
                self.write({
                    "success":False,
                    "msg":"删除失败！"
                })
        self.get_db().close()


class PraiseHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        _type = int(self.get_argument("type"))
        _live_id = self.get_argument("id")
        _user_id = self.get_user()["id"]

        db = self.get_db()
        sql_select = "SELECT id FROM praise WHERE type = %s and ref_live = %s and ref_user=%s"
        sql_insert = "INSERT INTO praise VALUES (%s, %s, %s, %s, %s)"

        if _type == 0:
            if db.get(sql_select, _type, _live_id, _user_id) is not None:
                self.write({
                    "success": False,
                    "msg": "您已经拍砖过了"
                })
            else:
                db.insert(sql_insert, None, _type, _user_id, _live_id, None)
                self.write({
                    "success": True
                })

        elif _type == 1:
            if db.get(sql_select, _type, _live_id, _user_id) is not None:
                self.write({
                    "success": False,
                    "msg": "您已经赞过了"
                })
            else:
                db.insert(sql_insert, None, _type, _user_id, _live_id, None)
                self.write({
                    "success": True
                })
        else:
            self.write({
                "success": True,
                "msg": "type值错误"
            })


class CommentHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        _live_id = self.get_argument("id")
        _response = self.get_argument("response", "")
        _user_id = self.get_user()["id"]

        if _response == "":
            self.write({
                "success": False,
                "msg": "评论文字不能为空"
            })
        elif len(_response) > 200:
            self.write({
                "success": False,
                "msg": "评论文字不能多余200个字"
            })
        else:
            db = self.get_db()
            sql_insert = "INSERT INTO comment VALUES (%s, %s, %s, %s, %s)"
            sql_select = "SELECT * FROM comment_view WHERE id=%s"

            comment_last_id = db.insert(sql_insert, None, _response, _user_id, _live_id, None)
            data = self.get_json(db.get(sql_select, comment_last_id))
            data["count"] = db.get("SELECT COUNT(id) as count FROM comment WHERE ref_live=%s", _live_id).count
            db.close()
            self.write({
                "success": True,
                "data": data
            })

