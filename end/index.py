__author__ = 'db'
# coding=utf-8
import base


class IndexHandler(base.BaseLiveHandler):
    def get(self):
        live_views = self.get_live_views()
        self.get_db().close()
        self.render("index.html", user=self.get_user(), live_views=live_views)


