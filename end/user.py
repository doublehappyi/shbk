__author__ = 'db'
# encoding=utf-8
import base
from bson.objectid import ObjectId
import tornado.web
import os.path
import datetime
import re
import settings


class RegisterHandler(base.BaseHandler):
    def get(self):
        self.render("register.html")

    def post(self, *args, **kwargs):
        username = self.get_argument("username")
        password = self.get_argument("password")
        password_again = self.get_argument("password_again")
        email = self.get_argument("email")
        agree = self.get_argument("agree", "false")

        if agree != "true":
            self.write("您尚未同意《用户协议》及《隐私政策》！")
            return

        print "agree is legal"

        if password != password_again:
            self.write("您输入的密码不一致！")
            return

        print "password is legal"

        db = self.get_db()

        if db.get("SELECT * FROM user WHERE username=%s", username) is not None:
            db.close()
            self.write("用户名已经被注册！")
            return

        print "username is legal"

        if db.get("SELECT * FROM user WHERE email=%s", email) is not None:
            db.close()
            self.write("邮箱已经被注册！")
            return

        print "email is legal"

        try:
            db.insert("INSERT INTO user VALUES (%s, %s, password(%s), %s, %s)", None, username, password, email,
                      "/static/avatars/default.jpg")
            db.close()
            self.write("注册成功")
        except:
            self.write("抱歉，数据库插入失败")


class UserHandler(base.BaseHandler):
    def get(self, _id):
        db = self.get_db()
        target_user_view = db.get("SELECT * FROM user_view WHERE id=%s", _id)
        live_views = db.query("SELECT * FROM live_view WHERE ref_user=%s", _id)
        db.close()
        self.render("user.html", user=self.get_user(), target_user_view=target_user_view, live_views=live_views)


class UserCommentHandler(base.BaseHandler):
    def get(self, _id, **kwargs):
        db = self.get_livebk()
        target_user = db.user.find_one({"_id": ObjectId(_id)})
        lives = []
        live_cursor = db.live.find()
        # 首先遍历所有live数据
        for live in live_cursor:
            # 取出live中的response数据
            response = live["response"]
            # 遍历response数据，如果发现response数据里面的user的值和当前的_id值相等，则这条live数据是该用户评论的
            for r in response:
                r_cursor = db.response.find_one({"_id": r})
                if str(r_cursor["user"]) == _id:
                    curr_live_cursor = db.live.find({"_id": r_cursor["live"]})
                    for curr_r in curr_live_cursor["response"]:
                        pass
                    lives.append()

                    break

        lives = db.live.find({"user": target_user["_id"]}).sort("photo_name", self.MONGO_DESCENDING)
        self.render("user-comment.html", user=self.get_user(), target_user=target_user, lives=lives)


class UserReplyHandler(base.BaseHandler):
    def get(self, _id, **kwargs):
        livebk = self.get_livebk()
        target_user = livebk.user.find_one({"_id": ObjectId(_id)})
        lives = livebk.live.find({"user": target_user["_id"]}).sort("photo_name", self.MONGO_DESCENDING)
        self.render("user-reply.html", user=self.get_user(), target_user=target_user, lives=lives)


class LoginHandler(base.BaseHandler):
    def get(self):
        self.render("login.html")

    def post(self, *args, **kwargs):
        username = self.get_argument("username")
        password = self.get_argument("password")
        next = self.get_argument("next", "/")

        print "next : " + next

        db = self.get_db()
        if db.get("SELECT * FROM user WHERE username=%s and password=password(%s)", username, password) is not None:
            self.set_secure_cookie("username", username)
            self.redirect(next)
        else:
            self.write("登录失败，用户名或密码错误！")


class LogoutHandler(base.BaseHandler):
    def post(self):
        self.clear_cookie("username")
        self.redirect("/")


class ModifyHandler(base.BaseHandler):
    @tornado.web.authenticated
    def get(self):
        db = self.get_db()
        user = self.get_user()
        user_view = db.get("SELECT * FROM user_view WHERE id=%s", user.id)

        db.close();
        self.render("modify.html", user=user, user_view=user_view)


class ModifyAvatarHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        avatar = self.request.files['avatar']
        uploadpath = os.path.join(settings.BASE_DIR, 'front', 'static', 'avatars')

        for meta in avatar:
            file_name = meta["filename"]
            file = meta['body']
        timestamp = re.compile("[\.:\- ]").sub("", str(datetime.datetime.now()))
        avatar_name = timestamp + "_" + file_name
        file_path = os.path.join(uploadpath, avatar_name)

        db = self.get_db()
        db.update("UPDATE user SET avatar=%s WHERE id=%s", "/static/avatars/" + avatar_name, self.get_user().id)
        db.close()

        with open(file_path, 'wb') as up:
            up.write(file)
            self.redirect("/modify")


class ModifyPasswordHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        password_old = self.get_argument("password_old")
        password = self.get_argument('password')
        password_again = self.get_argument("password_again")

        if password_old == password:
            self.write("新密码不能和当前密码相同")

        if password != password_again:
            self.write("您2次输入的密码不一致")
            return

        db = self.get_db()
        user = self.get_user()

        print "password old: " + password_old
        old_user = db.get("SELECT * FROM user WHERE username=%s and password=password(%s)", user.username, password_old)
        if old_user is not None:
            print "正在修改密码..."
            db.update("UPDATE user SET password=password(%s) WHERE id=%s", password, user.id)

            self.clear_cookie("username")
            self.redirect("/modify")
        else:
            self.write("当前密码错误")

        db.close()


class ModifyEmailHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        email = self.get_argument("email")
        db=self.get_db()
        db.update("UPDATE user SET email=%s WHERE id=%s", email, self.get_user().id)
        db.close()
        self.redirect("/modify")


class FocusHandler(base.BaseHandler):
    def post(self, *args, **kwargs):
        followed_id = int(self.get_argument("id"))

        if self.get_user() is None:
            self.write({"success": False, "msg": "您还未登录"})
            return

        following_id = self.get_user()["id"]
        if following_id == followed_id:
            self.write({
                "success": False,
                "msg": "您不能关注您自己"
            })
            return

        db = self.get_db()
        if db.get("SELECT id FROM follow WHERE followed=%s and following=%s", followed_id, following_id) is not None:
            self.write({
                "success": False,
                "focusing": True,
                "msg": "您已经关注该用户"
           })
        else:
            db.insert("INSERT INTO follow VALUES (%s, %s, %s, %s)", None, following_id, followed_id, None)
            self.write({
                "success": True
           })
        db.close()
        # if _type == 1:
        #     if db.focus.find_one({"master": _user_id, "guest": _guest_id}) is not None:
        #         self.write({
        #             "success": False,
        #             "focusing": True,
        #             "msg": "您已经关注该用户"
        #         })
        #         return
        #     db.focus.insert({
        #         "date": datetime.datetime.now(),
        #         "master": _user_id,
        #         "guest": _guest_id
        #     })
        #     self.write({
        #         "success": True
        #     })