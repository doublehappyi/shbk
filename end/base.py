__author__ = 'db'
# coding=utf-8
import tornado.web

import settings
import datetime
import json
import torndb


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.strftime("%Y-%m-%d %H:%M:%S")
        return json.JSONEncoder.default(self, o)


class BaseHandler(tornado.web.RequestHandler):
    def get_db(self):
        return torndb.Connection(settings.mysql_server, "shbk", user=settings.user, password=settings.password)

    def get_current_user(self):
        return self.get_secure_cookie("username")

    def get_user(self):
        user = self.get_db().get('SELECT * FROM user WHERE username=%s', self.get_secure_cookie("username"))
        self.get_db().close()
        return user

    def now(self):
        return datetime.datetime.now()

    def get_json(self, object):
        return json.loads(JSONEncoder().encode(object), encoding="UTF-8")


class BaseLiveHandler(BaseHandler):
    def get_live_views(self):
        if self.get_user() is not None:
            id = self.get_user().id
        else:
            id = None
        sql_select = 'SELECT *, (SELECT COUNT(id) FROM follow WHERE followed=live_view.ref_user and following=%s) as is_followed FROM live_view'
        live_views = self.get_db().query(sql_select, id)
        return live_views

    def get_live_view(self, live_id):
        if self.get_user() is not None:
            id = self.get_user().id
        else:
            id = None
        sql_select = 'SELECT *, (SELECT COUNT(id) FROM follow WHERE followed=live_view.ref_user and following=%s) as is_followed FROM live_view WHERE id=%s'
        live_views = self.get_db().get(sql_select, id, live_id)
        return live_views


