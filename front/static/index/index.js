/**
 * Created by db on 14-11-6.
 */
$(function () {
    bindEvents();
    function bindEvents() {
        praiseBind($(".good"), 1);
        praiseBind($(".bad"), 0);
        addFocus();
        deleteLive();
    }

    function praiseBind($dom, type, cb) {
        var praise_url = "/praise";
        $dom.click(function () {
            var $this = $(this),
                id = $this.attr("data-id");
            $.ajax(praise_url, {
                type: "POST",
                data: {
                    type: type,
                    id: id,
                    _xsrf: $.cookie("_xsrf")
                },
                success: function (data) {
                    console.log(data);
                    if (data.success) {
                        $this.next("span").text(parseInt($this.next("span").text()) + 1);
                    }
                },
                error: function () {

                }
            });
        });
    }

    function addFocus() {
        $(".j-focus").click(function () {
            var $this = $(this);
            var data = {
                _xsrf: $.cookie("_xsrf"),
                id: $this.attr("data-id")
            }
            $.ajax("/focus", {
                type: "POST",
                data: data,
                success: function (data) {
                    if (data.success || data.focusing) {
                        $this.unbind("click").text("已关注");
                    }else{
                        alert(data.msg)
                    }
                }
            })
        });
    }

    function deleteLive(){
        $(".j-delete").click(function(){
            var $this = $(this);
            var data = {
                _xsrf: $.cookie("_xsrf"),
                id:$this.attr("data-id")
            }

            $.ajax("/live/delete",{
                type:"POST",
                data: data,
                success:function(data){
                    if(data.success){
                        $this.closest(".live").remove();
                    }
                }
            });
        });
    }
});