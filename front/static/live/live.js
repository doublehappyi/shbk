/**
 * Created by db on 14-11-13.
 */
$(function () {
    $("#submit").click(function () {
        var data = {
            "id": $(this).attr("data-id"),
            "response": $("#response").val(),
            "_xsrf": $.cookie("_xsrf")
        }
        $.ajax("/comment", {
            data: data,
            type: "POST",
            success: function (data) {
                if (data.success) {
                    var data = data.data;
                    $('<div class="clear res">' +
                        '<div class="fl res-image">' +
                        '<a href="/user/' + data['ref_user'] + '">' +
                        '<img class="user-image" src="' + data["avatar"] + '">' +
                        '</a>' +
                        '</div>' +
                        '<div class="fl res-content">' +
                        '<a href="/user/' + data['ref_user'] + '">' +
                        '<span style="color: #2a6496">' + data['username'] + '</span>' +
                        '</a>' +

                        '<div>' + data['response'] + '</div>' +
                        '</div>' +
                        '<span class="fr res-floor" style="color: #ccc;width: 10%;">'+data["count"]+'楼</span>' +
                        '</div>' +
                        '<div style="text-align: right;color: #ccc;"><span>' + data["datetime"] + '</span></div>' +
                        '<hr/>').appendTo($("#response-container"));

                    $("#response").val("")
                }
            },
            error: function () {
            }
        })
    });
});