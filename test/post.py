__author__ = 'rt.yishuangxi'
# coding=utf8

import httplib, urllib

httpClient = None
DEVICE_ID = 16420
SENSOR_ID = 28358
HOST = "api.yeelink.net"
URL = "/v1.0/device/" + str(DEVICE_ID) + "/sensor/" + str(SENSOR_ID) + "/datapoints"
PORT = 80
DATA = '{"timestamp": "2012-03-15T16:13:14","value": 294.34}'
# DATA = {"name": "yisx"}
API_KEY = "b6b726c16e02ecb8e72ea0d3b81e1cd8"
HEADERS = {
    # "Content-type": "application/x-www-form-urlencoded",
    # "Accept": "text/plain",
    "U-ApiKey": API_KEY
}
try:
    # params = urllib.urlencode({'name': 'tom', 'age': 22})
    # headers = {"Content-type": "application/x-www-form-urlencoded"
    # , "Accept": "text/plain"}
    httpClient = httplib.HTTPConnection(HOST, PORT, timeout=30)
    httpClient.request("POST", URL, DATA, HEADERS)

    response = httpClient.getresponse()
    # print "status: "+ str(response.status)
    # print "reason: "+str(response.reason)
    # print "response.read(): "+str(response.read())
    print "response.getheaders(): "+str(response.getheaders())  # 获取头信息
except Exception, e:
    print e
finally:
    if httpClient:
        httpClient.close()