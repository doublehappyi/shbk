__author__ = 'rt.yishuangxi'
import tornado.web
import tornado.ioloop


class MainHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.write("hello get")
    def post(self, *args, **kwargs):
        self.write("hello post")


if __name__ == "__main__":
    app = tornado.web.Application([("/", MainHandler)])
    app.listen(8000)
    tornado.ioloop.IOLoop.instance().start()